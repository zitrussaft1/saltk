local MouseBasic = require("scripts/saltk/Core/Mouse/Basic")

local Widget = {}
Widget.__index = Widget
setmetatable(Widget,MouseBasic)

function Widget.new(props)
    local instance = setmetatable(MouseBasic.new(props), Widget)
    instance.on_root_update = Widget.on_root_update
    return instance
end

local last_mouse_x, last_mouse_y = 0, 0
function Widget:on_root_update()
    MouseBasic.on_root_update(self)

    local mouse_x, mouse_y = sol.input.get_mouse_position()
    if mouse_x ~= last_mouse_x and mouse_y ~= last_mouse_y then
        local hovered = self:find_child_mouse_hover(mouse_x, mouse_y)
        if hovered and hovered ~= self.root.focused_widget then
            hovered:set_focus(true)
        end
    end
    last_mouse_x, last_mouse_y = mouse_x, mouse_y
end

return Widget