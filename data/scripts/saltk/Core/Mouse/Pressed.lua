local MouseBasic = require("scripts/saltk/Core/Mouse/Basic")

local Widget = {}
Widget.__index = Widget
setmetatable(Widget, MouseBasic)

function Widget.new(props)
    local instance = setmetatable(MouseBasic.new(props), Widget)
    return instance
end

function Widget:on_mouse_pressed(button, x, y)
    local child = self:find_child_mouse_hover(x, y)
    if child then
        child:signal_handler("clicked", button, x, y)
    end
end

return Widget