local Image = require("scripts/saltk/Core/Display/Image")

local Widget = {}
Widget.__index = Widget
setmetatable(Widget, Image)

function Widget.new(props)
    Image.set_image = Widget.set_image
    local instance = setmetatable(Image.new(props), Widget)

    return instance
end

function Widget:set_image(image)
    assert(type(image) == "string" or 
        (type(image) == "userdata" and
        image.get_animation_set ~= nil)
        , "Widget:set_image(image) requires a string or a sprite")


    if type(image) == "string" then
        self.image.surface = sol.sprite.create(image)
        assert(self.image.surface ~= nil, "Widget:set_image(image) requires a valid sprite")
    else
        self.image.surface = image
    end

    local width, height = self.image.surface:get_size()
    
    self.image.width = width
    self.image.height = height
    self.queue_draw = true
end

function Widget:draw(surface)
    self.surface:clear()
    self:draw_crop()
    Image.draw(self, surface)
end

return Widget