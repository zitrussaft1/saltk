
local utils = {}

function utils.rgb_to_hsv(color)
    local r = color[1]
    local g = color[2]
    local b = color[3]
    local max = math.max(r, g, b)
    local min = math.min(r, g, b)
    local h = 0
    local s = 0
    local v = max
    local d = max - min
    if max ~= 0 then
        s = d / max
    end
    if max == min then
        h = 0
    elseif max == r then
        h = (g - b) / d
        if g < b then
            h = h + 6
        end
    elseif max == g then
        h = (b - r) / d + 2
    elseif max == b then
        h = (r - g) / d + 4
    end
    h = h / 6
    return {h, s, v}
end

function utils.hsv_to_rgb(color)
    local h = color[1]
    local s = color[2]
    local v = color[3]
    local r = 0
    local g = 0
    local b = 0
    local i = math.floor(h * 6)
    local f = h * 6 - i
    local p = v * (1 - s)
    local q = v * (1 - f * s)
    local t = v * (1 - (1 - f) * s)
    i = i % 6
    if i == 0 then
        r = v
        g = t
        b = p
    elseif i == 1 then
        r = q
        g = v
        b = p
    elseif i == 2 then
        r = p
        g = v
        b = t
    elseif i == 3 then
        r = p
        g = q
        b = v
    elseif i == 4 then
        r = t
        g = p
        b = v
    elseif i == 5 then
        r = v
        g = p
        b = q
    end
    return {r, g, b}
end

return utils
