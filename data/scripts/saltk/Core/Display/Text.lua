local Basic = require("scripts/saltk/Core/Display/Basic")

local Widget = {}
Widget.__index = Widget
setmetatable(Widget, Basic)

function Widget.new(props)
    local instance = setmetatable(Basic.new(props), Widget)
    
    Widget.set_text(instance, props.text)
    Widget.set_color(instance, props.color or {0,0,0})
    Widget.set_font(instance, props.font or "8_bit")
    Widget.set_font_size(instance, props.font_size or 12)
    Widget.set_rendering_mode(instance, props.rendering_mode or "solid")


    return instance

end

function Widget:set_text(text)
    assert(type(text) == "string", "Widget:set_text(text) requires a string")
    self.text = text
    if self.text_render then
        self.text_render.surface:set_text(text)
    end
end

function Widget:get_text()
    return self.text
end

function Widget:set_font(font)
    assert(type(font) == "string", "Widget:set_font(font) requires a string")
    self.font = font
    self.queue_draw = true
end

function Widget:set_font_size(font_size)
    assert(type(font_size) == "number", "Widget:set_font_size(font_size) requires a number")
    self.font_size = font_size
    self.queue_draw = true
end

function Widget:set_color(color)
    assert(type(color) == "table" and
        type(color[1]) == "number" and
        type(color[2]) == "number" and
        type(color[3]) == "number",
        "Widget:set_color(color) requires a table with 3 numbers")
    self.color = color
    self.queue_draw = true
end

function Widget:set_rendering_mode(rendering_mode)
    assert(rendering_mode == "solid" or rendering_mode == "antialiasing",
        "Widget:set_rendering_mode(rendering_mode) requires a string 'solid' or 'antialiasing'")
    self.rendering_mode = rendering_mode
    self.queue_draw = true
end


function Widget:render()
    Basic.render(self)

    local font_size = self.font_size

    if self.scale then
        local window_width, window_height = sol.video.get_window_size()
        local quest_width, quest_height = sol.video.get_quest_size()
        local scale_x = window_width / quest_width
        local scale_y = window_height / quest_height
        font_size = self.font_size * scale_x
    end

    self.text_render = {}
    self.text_render.surface = sol.text_surface.create({
        font = self.font,
        font_size = font_size,
        color = self.color,
        rendering_mode = self.rendering_mode,
        text = self.text,
    })

    local width, height = self.text_render.surface:get_size()
    self.text_render.width = width
    self.text_render.height = height

    self.text_render.surface:draw(self.surface, 0, font_size/2)
    self.surface:set_opacity(self.opacity)
end

return Widget