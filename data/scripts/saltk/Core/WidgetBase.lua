local Widget = {}
Widget.__index = Widget

function Widget.new(props)
    local instance = setmetatable({}, Widget)

    instance.id = props.id or "root"
    instance.visible = props.visible or true
    instance.sensitive = props.sensitive or false
    instance.has_focus = props.has_focus or false
    instance.size = props.size or {320, 240}
    instance.position = props.position or {0, 0}
    instance.signals = props.signals or {
        focused = function(Widget) print("focused", Widget.id) end,
        unfocused = function(Widget) print("unfocused", Widget.id) end,
    }
    instance.queue_draw = false
    instance.children = props.children or nil
    instance.focused_widget = props.focused_widget or nil
    instance.parent = props.parent or nil
    instance.scale = props.scale or false
    instance.index = props.index or 1
    instance.root = props.root or instance

    Widget.set_size(instance, instance.size)
    Widget.set_position(instance, instance.position)

    return instance
end

function Widget:add_signal(signal, def)
    assert(type(signal) == "string", "Widget:add_signal(signal, def) signal must be a string")
    assert(type(def) == "function", "Widget:add_signal(signal, def) def must be a function")
    local f = self.signals[signal]
    if f then
        self.signals[signal] = function (...)
            if not f(...) then
                return def(...)
            end
        end
    else
        self.signals[signal] = def
    end
end

function Widget:signal_handler(signal, ...)
    assert(type(signal) == "string", "Widget:signal_handler(signal, ...) signal must be a string")
    if self.signals[signal] then
        self.signals[signal](self, ...)
    else
        print("WARNING: No signal handler for " .. signal)
    end
end

function Widget:show()
    self.visible = true
end

function Widget:hide()
    self.visible = false
end

function Widget:is_visible()
    return self.visible
end

function Widget:set_sensitive(sensitive)
    assert(type(sensitive) == "boolean", "Widget:set_sensitive(bool) must be a boolean")
    self.sensitive = sensitive
end

function Widget:is_sensitive()
    return self.sensitive
end

function Widget:set_size(size)
    assert(type(size) == "table" and
        type(size[1]) == "number" and
        type(size[2]) == "number" and
        size[1] > 0 and size[2] > 0,
        "Widget:set_size({x,y}) must be a table of two positive numbers")


    local width = size[1]
    local height = size[2]

    if self.scale then
        local window_width, window_height = sol.video.get_window_size()
        local quest_width, quest_height = sol.video.get_quest_size()
        local scale_x = window_width / quest_width
        local scale_y = window_height / quest_height

        width = width * scale_x
        height = height * scale_x
    end

    self.width = width
    self.height = height

    self.surface = sol.surface.create(self.width, self.height)
end

function Widget:get_size()
    return self.width, self.height
end

function Widget:set_position(position, modify)
    
    assert(type(position) == "table" and
        type(position[1]) == "number" and
        type(position[2]) == "number",
        "Widget:set_position({x,y}) must be a table of two numbers")
    if modify == nil then
        modify = true
    end

    local x = position[1]
    local y = position[2]
    if self.parent and modify then
        x = x + self.parent.position[1]
        y = y + self.parent.position[2]
    end

    self.position = {x, y}

    if self.scale then
        local window_width, window_height = sol.video.get_window_size()
        local quest_width, quest_height = sol.video.get_quest_size()
        local scale_x = window_width / quest_width
        local scale_y = window_height / quest_height
        x = x * scale_x
        y = y * scale_x
    end
    
    self.x = x
    self.y = y
end

function Widget:get_position()
    return self.position
end

local last_window_width, last_window_height = 0,0
function Widget:on_root_update()
    local window_width, window_height = sol.video.get_window_size()
    if window_width ~= last_window_width or window_height ~= last_window_height then

        local function redraw(widget)
            widget.queue_draw = true
            Widget.set_size(widget, widget.size)
            
            for _, child in pairs(widget.children or {}) do
                redraw(child)
            end
        end
        redraw(self)

        last_window_width, last_window_height = window_width, window_height
    end
end

function Widget:draw(surface)
    if not self.visible or not sol.menu.is_started(self.root) then
        return
    end
    if self.id == self.root.id then
        self:on_root_update()
    end

    if self.queue_draw then
        self:render()
        self.queue_draw = false
    end
    self.surface:draw(surface, self.x, self.y)
    
    for _, child in pairs(self.children or {}) do
        child:draw(self.root.surface)
    end
end

function Widget:render()
    self:set_position(self.position, false)
end

function Widget:add_child(child)
    self.children = self.children or {}
    child.parent = self
    child.scale = self.scale

    if self.root == nil then
        child.root = self
    else
        child.root = self.root
    end
    child:set_position(child.position)
    child.index = #self.children + 1
    child.queue_draw = true
    table.insert(self.children, child)
end

function Widget:remove_child(child)
    if self.children then
        for i, c in pairs(self.children) do
            if c == child then
                table.remove(self.children, i)
                break
            end
        end
    end
end

function Widget:remove_all_children()
    self.children = {}
end

function Widget:get_children()
    return self.children
end

function Widget:get_child_by_id(id)
    if self.children then
        for _, child in pairs(self.children) do
            if child.id == id then
                return child
            end
        end
    end
    return nil
end

function Widget:get_child_by_index(index)
    if self.children then
        return self.children[index]
    end
    return nil
end

function Widget:get_child_index(child)
    if self.children then
        for i, c in pairs(self.children) do
            if c == child then
                return i
            end
        end
    end
    return nil
end

function Widget:remove_child_by_id(id)
    if self.children then
        for i, child in pairs(self.children) do
            if child.id == id then
                table.remove(self.children, i)
                break
            end
        end
    end
end

function Widget:remove_child_by_index(index)
    if self.children then
        table.remove(self.children, index)
    end
end

function Widget:set_focus(bool)
    assert(type(bool) == "boolean", "Widget:set_focus(bool) must be a boolean")
    if self ~= self.root.focused_widget and bool then
        if self.root.focused_widget then
            self.root.focused_widget:set_focus(false)
        end
        self.has_focus = true
        self.root.focused_widget = self
        self:signal_handler("focused")
    elseif self == self.root.focused_widget and not bool then
        self.has_focus = false
        self.root.focused_widget = nil
        self:signal_handler("unfocused")
    end
end

function Widget:get_focus()
    return self.has_focus
end

return Widget