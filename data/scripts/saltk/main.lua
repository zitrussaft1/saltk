local saltk = {
    WidgetColor = require("scripts/saltk/Default/WidgetColor"),
    Image = require("scripts/saltk/Default/Image"),
    Sprite = require("scripts/saltk/Default/Sprite"),
    Text = require("scripts/saltk/Default/Text"),
}
local menus_to_draw = {}

function saltk.init()
    local prev_menu_start = sol.menu.start

    sol.menu.start = function (context, menu, ...)
        if menu.scale then
            menus_to_draw[menu.id] = menu
        else
            menu.on_draw = menu.draw
        end
        prev_menu_start(context, menu, ...)
    end

    local prev_video_draw = sol.video.on_draw

    sol.video.on_draw = function (self, dst_surface)
        for _, menu in pairs(menus_to_draw) do
            menu:draw(dst_surface)
        end
        if prev_video_draw then
            prev_video_draw(self, dst_surface)
        end
    end
    return true
end

return saltk