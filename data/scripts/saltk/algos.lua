local algos = {}

function algos.deep_merge(t1, t2)
    for k, v in pairs(t2) do
        if type(v) == "table" and k ~= "root" and k ~= "parent" then
            if type(t1[k] or false) == "table" then
                algos.deep_merge(t1[k] or {}, t2[k] or {})
            else
                t1[k] = v
            end
        else
            t1[k] = v
        end
    end
end

function algos.print_table(table, indent)
    indent = indent or 0
    for k, v in pairs(table) do
        local formatting = string.rep("  ", indent) .. k .. ": "
        if k == "root" or k == "parent" then
            print(v)
        elseif type(v) == "table" then
            print(formatting)
            algos.print_table(v, indent + 1)
        else
            print(formatting .. tostring(v))
        end
    end
end

return algos