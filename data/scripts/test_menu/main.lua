local saltk = require("scripts/saltk/main")

function setup_menu(game, hp1, hp2)
    local root = saltk.Widget.new({id="root", size={320, 240},
        position={0, 0}, scale=true, can_be_selected=false,
        opacity=100, has_focus=true
    })
    root.hp1 = hp1
    root.hp2 = hp2

    local fakemon1 = saltk.Image.new({
        id="1", image = "fakemon/ground.png",
        size={128,128}, position={5,60}
    })

    local fakemon2 = saltk.Image.new({
        id="2", image = "fakemon/steel.png",
        size={128,128}, position={120,5},
    })

    local explosion_mon1 = saltk.Image.new({
        id="explosion_mon1", image = "fakemon/explosion.png",
        size={128,128}, position={5,60},
        visible = false
    })

    local explosion_mon2 = saltk.Image.new({
        id="explosion_mon2", image = "fakemon/explosion.png",
        size={128,128}, position={120,5},
        visible = false
    })

    local actions_grid = saltk.Grid.new({
        id="options",
        grid={1,4}, padding={4,4},
        size={80, 160}, position={320-80, 240-160}
    })

    local grid_bg = saltk.Widget.new({
        id="bg",
        size={80, 160}, position={320-80, 240-160},
        background_color={0,0,255}
    })

    local battle_log = saltk.Text.new({
        id="battle_log", halign="left",
        valign="top", text_overflow="...",
        overflow=true, font_size=24,
        text="Choose a skill",
        padding={0,0},
        size={320-80, 240-160}, position={0,160}
    })

    local battle_log_bg = saltk.Widget.new({
        id="battle_log_bg",
        size={320-80, 240-160}, position={0,160},
        background_color={125,125,0}
    })

    saltk.Widget.add_child(root, fakemon1)
    saltk.Widget.add_child(root, fakemon2)
    saltk.Widget.add_child(root, explosion_mon1)
    saltk.Widget.add_child(root, explosion_mon2)
    saltk.Widget.add_child(root, grid_bg)
    saltk.Widget.add_child(root, actions_grid)
    saltk.Widget.add_child(root, battle_log_bg)
    saltk.Widget.add_child(root, battle_log)

    local function close()
        local time = 50
        sol.timer.start(game, 1000, function ()
            root.surface:fade_out(time, function () end)
            fakemon1.surface:fade_out(time, function () end)
            fakemon2.surface:fade_out(time, function () end)
            grid_bg.surface:fade_out(time, function () end)
            actions_grid.surface:fade_out(time, function () end)
            battle_log_bg.surface:fade_out(time, function () end)
            battle_log.surface:fade_out(time, function () 
                game:set_saltk_enabled("test", false)
            end)
        end)
    end

    local actions = {
        saltk.Text.new({id="fight", halign="center",
            valign="middle", text_overflow="...",
            overflow=false, font_size=8,
            text="Fight",
            padding={0,0},
            can_be_selected=true,
            on_key_press = function (widget, key)
                if key == "x" then
                    if root.hp1 > 0 then
                        root.hp1 = root.hp1 - 5
                        battle_log.text = "Caused 5 damage to fakemon"
                        explosion_mon1.visible = true
                        sol.timer.start(game, 1000, function ()
                            explosion_mon1.visible = false
                        end)
                    else
                        battle_log.text = "Fakemon fainted!"
                        close()
                    end
                end
            end
        }),
        saltk.Text.new({id="heal", halign="center",
            valign="middle", text_overflow="...",
            overflow=false, font_size=8,
            text="Heal",
            padding={0,0},
            can_be_selected=true,
            on_key_press = function (widget, key)
                if key == "x" then
                    if root.hp2 > 0 then
                        root.hp2 = root.hp2 - 5
                        battle_log.text = "Enemy took opportunity and hit you -5 HP"
                        explosion_mon2.visible = true
                        sol.timer.start(game, 1000, function ()
                            explosion_mon2.visible = false
                        end)
                    else
                        battle_log.text = "Fakemon fainted!"
                        close()
                    end
                end
            end
        }),
        saltk.Text.new({id="surrender", halign="center",
            valign="middle", text_overflow="...",
            overflow=false, font_size=8,
            text="Surrender",
            padding={0,0},
            can_be_selected=true,
            on_key_press = function (widget, key)
                if key == "x" then
                    root.hp1 = 0
                    battle_log.text = "You surrendered!"
                    close()
                end
            end
        }),
        saltk.Text.new({id="win", halign="center",
            valign="middle", text_overflow="...",
            overflow=false, font_size=8,
            text="Win",
            padding={0,0},
            can_be_selected=true,
            on_key_press = function (widget, key)
                if key == "x" then
                    root.hp2 = 0
                    battle_log.text = "You won!"
                    close()
                end
            end
        }),
    }

    for i in ipairs(actions) do
        saltk.Grid.add_child(actions_grid, actions[i])
    end

    saltk.init(game, "test", root, true)

end



return setup_menu